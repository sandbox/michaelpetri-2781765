Styled image
============

In some cases it's necessary to select the image style while creating content.
In the field widget settings you can select if you want to support image styles or responsive image styles.
The field formatter is simple fork of the core image formatter except that it handles both image styles and responsive image styles.
