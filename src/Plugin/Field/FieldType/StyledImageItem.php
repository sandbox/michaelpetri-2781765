<?php

namespace Drupal\styled_image\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\image\Plugin\Field\FieldType\ImageItem;

/**
 * Plugin implementation of the 'style_image_item' field type.
 *
 * @FieldType(
 *   id = "styled_image",
 *   label = @Translation("Styled image"),
 *   category = @Translation("Reference"),
 *   description = @Translation("An image field with additional image style information."),
 *   default_widget = "styled_image_widget",
 *   default_formatter = "styled_image_formatter",
 *   column_groups = {
 *     "file" = {
 *       "label" = @Translation("File"),
 *       "columns" = {
 *         "target_id", "width", "height"
 *       },
 *       "require_all_groups_for_translation" = TRUE
 *     },
 *     "alt" = {
 *       "label" = @Translation("Alt"),
 *       "translatable" = TRUE
 *     },
 *     "title" = {
 *       "label" = @Translation("Title"),
 *       "translatable" = TRUE
 *     },
 *   },
 *   list_class = "\Drupal\file\Plugin\Field\FieldType\FileFieldItemList",
 *   constraints = {"ReferenceAccess" = {}, "FileValidation" = {}}
 * )
 */
class StyledImageItem extends ImageItem {

  /**
   * @return \Drupal\Core\TypedData\ComplexDataDefinitionInterface
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['image_style_type'] = DataDefinition::create('string')
      ->setLabel(t('Image style type'))
      ->setDescription(t('Defines which image style type should be used in formatter.'));

    $properties['image_style'] = DataDefinition::create('string')
      ->setLabel(t('Image style'))
      ->setDescription(t('The image style that should be used in formatter.'));

    return $properties;
  }

  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['image_style_type'] = [
      'description' => 'The name of the image style type.',
      'type' => 'varchar',
      'length' => 512,
    ];
    $schema['columns']['image_style'] = [
      'description' => 'The name of the image style.',
      'type' => 'varchar',
      'length' => 512,
    ];

    return $schema;
  }
}
